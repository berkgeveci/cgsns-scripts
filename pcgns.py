from paraview.util.vtkAlgorithm import *
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonCore import vtkDataArraySelection
from vtkmodules.vtkIOCGNSReader import vtkCGNSReader
import os
from mpi4py import MPI

def createModifiedCallback(anobject):
    import weakref

    weakref_obj = weakref.ref(anobject)
    anobject = None

    def _markmodified(*args, **kwars):
        o = weakref_obj()
        if o is not None:
            o.Modified()

    return _markmodified


@smproxy.reader(name="pCGNSReader", label="Python-based CGNS Reader",
                extensions="cgnss", file_description="CGNS Files")
class pCGNSReader(VTKPythonAlgorithmBase):

    def __init__(self):
        """In the constructor, we initialize internal variables"""
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkPartitionedDataSet"
        )
        self._filename = None

        self._parrayselection = vtkDataArraySelection()
        self._parrayselection.AddObserver("ModifiedEvent", createModifiedCallback(self))

        self._carrayselection = vtkDataArraySelection()
        self._carrayselection.AddObserver("ModifiedEvent", createModifiedCallback(self))

    @smproperty.stringvector(name="FileName")
    @smdomain.filelist()
    @smhint.filechooser(extensions="cgnss", file_description="CGNS meta files")
    def SetFileName(self, name):
        if self._filename != name:
            self._filename = name
            self.Modified()

    def _getarrays(self, sel):
        arrays = []
        narrays = sel.GetNumberOfArrays()
        for i in range(narrays):
            arrays.append(sel.GetArrayName(i))
        return arrays

    @smproperty.dataarrayselection(name="PointArrays")
    def GetPointDataArraySelection(self):
        return self._parrayselection

    @smproperty.dataarrayselection(name="CellArrays")
    def GetCellDataArraySelection(self):
        return self._carrayselection

    def RequestInformation(self, request, inInfoVec, outInfoVec):
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()

        if rank == 0:
            dname = os.path.dirname(self._filename)
            with open(self._filename) as f:
                lines = f.readlines()
            fnames = list(map(lambda x : dname+"/"+x.strip(), lines))

            from vtkmodules.vtkParallelCore import vtkDummyController
            r = vtkCGNSReader()
            r.SetController(vtkDummyController())
            r.SetFileName(fnames[0])
            r.UpdateInformation()

            psel = r.GetPointDataArraySelection()
            parrays = self._getarrays(psel)
            csel = r.GetCellDataArraySelection()
            carrays = self._getarrays(csel)
        else:
            parrays = None
            carrays = None

        parrays = comm.bcast(parrays, root=0)
        for array in parrays:
            self._parrayselection.AddArray(array)            
        carrays = comm.bcast(carrays, root=0)
        for array in carrays:
            self._carrayselection.AddArray(array)            

        return 1


    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonDataModel import vtkPartitionedDataSet
        from vtkmodules.vtkParallelMPI import vtkMPIController
        from vtkmodules.vtkParallelCore import vtkProcessGroup

        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()

        if rank == 0:
            dname = os.path.dirname(self._filename)
            with open(self._filename) as f:
                lines = f.readlines()
            fnames = list(map(lambda x : dname+"/"+x.strip(), lines))
        else:
            fnames = None
        fnames = comm.bcast(fnames, root=0)

        nfiles = len(fnames)

        nranks_per_file = 1
        if size > nfiles:
            nranks_per_file = size // nfiles
            if size % nfiles != 0:
                print("The MPI size must be divisable by the number of files")
                return 0

        contr = vtkMPIController()

        if nranks_per_file == 1:
            nfiles_per_rank = nfiles // size
            fnames = fnames[rank*nfiles_per_rank:(rank+1)*nfiles_per_rank]
        else:
            nfiles2 = size // nranks_per_file
            scontrs = []
            for i in range(nfiles2):
                pg = vtkProcessGroup()
                pg.Initialize(contr)
                pg.RemoveAllProcessIds()
                for j in range(nranks_per_file):
                    pg.AddProcessId(i*nranks_per_file+j)
                scontr = contr.CreateSubController(pg)
                scontrs.append(scontr)
            fnames = [fnames[rank//nranks_per_file]]
        
        output = vtkPartitionedDataSet.GetData(outInfoVec, 0)

        from vtkmodules.vtkParallelCore import vtkDummyController
        dc = vtkDummyController()

        blockId = 0
        for i, fname in enumerate(fnames):
            r = vtkCGNSReader()
            if nranks_per_file == 1:
                r.SetController(dc)
            else:
                r.SetController(scontrs[rank//nranks_per_file])
            r.SetFileName(fnames[i])
#            r.UpdateInformation()
#            print(f"rank {rank} is reading {fnames[i]}")
            r.GetPointDataArraySelection().CopySelections(self._parrayselection)
            r.GetCellDataArraySelection().CopySelections(self._carrayselection)
#            print(nranks_per_file)
            if nranks_per_file == 1:
                r.Update()
            else:
                contr = scontrs[rank//nranks_per_file]
                r.UpdatePiece(contr.GetLocalProcessId(), contr.GetNumberOfProcesses(), 0)
            mb = r.GetOutputDataObject(0)
            main = mb.GetBlock(0)
            nblocks = main.GetNumberOfBlocks()
            for j in range(nblocks):
                o = main.GetBlock(j)
                if o:
                    o2 = o.NewInstance()
                    o2.ShallowCopy(o)
                    output.SetPartition(blockId, o2)
                    blockId += 1

        return 1
